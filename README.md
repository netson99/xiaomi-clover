# Xiaomi MI PAD 4 / Clover

kernel source : https://github.com/ubuntu-touch-clover/halium_kernel_xiaomi_clover.git

Rootfs Image : https://ci.ubports.com/job/focal-hybris-rootfs-arm64/job/master/824/artifact/ubuntu-touch-android9plus-rootfs-arm64.tar.gz

Halium Image : https://ci.ubports.com/job/UBportsCommunityPortsJenkinsCI/job/ubports%252Fporting%252Fcommunity-ports%252Fjenkins-ci%252Fgeneric_arm64/job/main/lastSuccessfulBuild/artifact/halium_halium_arm64.tar.xz

Vendor image : vendor.img

Local build :
-- prepare the host pc as per the ubports docs.
mkdir -o focal-dev/{build,ota,out}
#build boot.img and system.img with the rootfs and halium from the lastSuccessfulBuild
./build-focal-gsi-dev.sh

#copy boot.img and system.img to mi pad 4
# via TWRP install boot.img to BOOT and system.img to SYSTEM
# and vendor.img to VENDOR (only once)
# Reboot to System and wait for the logging Process

